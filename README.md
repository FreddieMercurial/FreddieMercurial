### Hi there 👋

<!-- CUSTOM-BADGES:START -->
![GitHub followers](https://img.shields.io/github/followers/FreddieMercurial?style=social) 
[![Twitter Follow](https://img.shields.io/twitter/follow/JessicaMulein?style=social)](https://www.twitter.com/JessicaMulein)
<!-- CUSTOM-BADGES:END -->

- 🔭 I've recently started an open source team called "[Digital Defiance](https://digitaldefiance.org)" which has absorbed most of my projects at least in theory. Right now we're actively working on [HACC](https://github.com/Blazor-Console/HACC) which is nearing/in Beta!
  - HACC is just reaching a really really usable state!
    ![ezgif com-gif-maker (1)](https://user-images.githubusercontent.com/3766240/169591501-52acb96f-d6f3-44b5-b6af-cffe7731c713.gif)
    
- I work on a lot of side projects, always starting new things. I usually circle back to projects eventually.

- I’m also working on [qURI](https://github.com/BrightChain/quri) (ranking/rating component of BrightChain), [Domino Train](https://github.com/FreddieMercurial/Domino-Train) (Firebase Blazor webassembly demo project), [PhantomMail](https://github.com/PhantomMail) (CLI email client) which will run in HACC and WordMasterMind (Wordle clone with multiple dictionaries, more words/sizes), [BrightChain](https://github.com/BrightChain/BrightChain) + NeuralFabric, HACC, PlayZMachine mainly. Always cycling back to these unfinished projects. Many of them have actually merged into BrightChain- it's bigger than anyone I think can imagine. However, I'm disabled and I am more of an ideas person these days, trying to explore some concepts. Other projects include Bailey Borwein Plouffe and LUHN-Mod-N.
  - Domino Train is Webassembly with EF/MS OIDC integrated OAUTH from Firebase, uses firebase to handle the multiplayer functionality, and CSS to draw the dominoes.
   ![image](https://user-images.githubusercontent.com/3766240/170848910-fb6e8e0d-eb9a-4606-87ac-ce4cd685c696.png)
  - [PhantomMail](https://github.com/PhantomMail) is Gui.cs (and soon likely HACC) based modern retro email client.
    ![image](https://user-images.githubusercontent.com/3766240/155912631-7904ef2c-e093-44a1-b4cb-9017721caf43.png)

- My old projects aren't abandoned- though some are kind of ideas I've thought about and may interest you, so I've left whatever work (if any) I've done. 🤷‍♀️

- 🌱 I’m always learning. I'm rebuilding my life as a software engineer and musician after losing many years and a lot of ability to autoimmune diseases and then COVID/Long COVID which further provoked the autoimmune issues. I always am trying to stay current in a lot of different areas. Pushing my knowledge on C#/Blazor, JavaScript, TypeScript and more.

- 👯 I’m looking to collaborate on PhantomMail, WordMasterMind, BrightChain, StegoPhone, HACC (HTML5 ANSI Console Canvas), PlayZMachine (Zork on console/twitter bot), and more.

- 💬 Ask me about Software Engineering, Music, Writing. Programmer since ~1987, professional since 1994. Embedded systems, DSP, Web/Full Stack. C/C++, Python, PHP, C#, Java, JavaScript/TypeScript/ECMAScript, ActionScript/Flex, Perl, Go, Pascal, and more.

- 📫 How to reach me: jessica [at] mulein [dot] com
  - Alternate emails: jessica [at] mulein [dot] com [dot] ua (Ukraine/Украї́нська)
  - https://jessicamulein.com (GitHub pages hosted)
  - https://hashnode.jessicamulein.com
  - https://polywork.jessicamulein.com 
  - <a rel="me" href="https://fivebyfive.social/@jmulein">Mastodon</a> or <a rel="me" href="https://tech.lgbt/@jmulein">Alt Mastodon</a>

- 😄 Pronouns: Mercurial/Highness/Majesty (she/her if you really want to know)

- ⚡ Fun fact: I was once a licensed coal miner and have coded in the deepest darkest places on earth. One of my names is on millions of bookshelves and I have friends in high places. I am Mercurial.

- By the way, Mercurial is a play on words related to software revision control systems like Git an Mercurial and then of course Freddie Mercury.
- Pseudonym is because people ignore women.

![Jessica's GitHub stats](https://github-readme-stats.vercel.app/api?username=freddiemercurial&theme=tokyonight)
